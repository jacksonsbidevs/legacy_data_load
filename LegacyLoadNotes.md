# Legacy Data Load Notes

Transaction data from January 2019 to the middle of May 2019 comes from a "legacy" system.

The main source are:

1. The SQLServer table `[EMCS_Staging].[dbo].[SalesTransactionItemStoreMinute2019]`
2. Compressed `gz` files in the directory `\\jfscorpftp01\ftparchive$\ExtraMileHistory\Dump`

The keys in these data are not natural hence stores, items and categories must be matched with
BlueCube transaction data which is stored in `[EMCS_Staging].[dbo].[stage_ST_TransactionItemSales]`.
These notes outline the load steps.

*All referenced SQL scripts can be found the BitBucket repository*

[https://bitbucket.org/jacksonsbidevs/daily_xml_preprocess/src/master/sqlhacks/](https://bitbucket.org/jacksonsbidevs/daily_xml_preprocess/src/master/sqlhacks/)


1. **Import Data** for `gz` files use the SSIS package `legacy flat file load` to load 
   uncompressed `csv` files to temporary month files like `SalesTransactionItemStoreMinute2019mar`.

2. Apply basic data checks to compare the newly loaded data to existing tables.
   Remove ridiculous outliers like 50 million dollar sales.
   
    ```SQL
        select avg(GrossSoldAmount) as amt, stdev(GrossSoldAmount) as std_amt,
            avg(GrossSoldQuantity) as qty, stdev(GrossSoldQuantity) as std_qty
        from [dbo].[SalesTransactionItemStoreMinute2019]
    union all
        select avg(GrossSoldAmount), stdev(GrossSoldAmount),
            avg(GrossSoldQuantity), stdev(GrossSoldQuantity)
        from [dbo].[SalesTransactionItemStoreMinute2019mar]
   ```

3. **Convert to Blue Cube temp format** run the SQL `legacy_sales_transactions.sql` to convert 
   the legacy transaction to staging temp format. Edit the script to point to the proper source table.


4. **Map legacy stores**

    ```SQL
        -- store number buCode xref for month legacy data
        drop table if exists stage_ST_Legacy_Store_Xref2;
        select distinct c.StoreNumber, c.FranchiseID, a.buCode
        into stage_ST_Legacy_Store_Xref2
        from raw_LegacyData_Temp a
        inner join LookupStore b
        on a.buCode = b.StoreKey
        inner join vEMCSFacility c
        on try_cast(c.FranchiseID as int) = try_cast(b.StoreNumber as int);

        -- update store numbers in legacy data
        update t
        set t.BuId = s.StoreNumber
        from raw_LegacyData_Temp as t
        inner join stage_ST_Legacy_Store_Xref2 as s
        on  t.buCode = s.buCode
    ```

5. **Map items and categories** work through the SQL script `legacy_unified_item_xref.sql`
   to map items and categories. Requires manual tweaking.

   ```SQL
        drop table if exists [dbo].raw_LegacyData_TransactionItemSales_Temp_Mapped;
        select *
        into [dbo].raw_LegacyData_TransactionItemSales_Temp_Mapped
        from raw_LegacyData_Temp;

        -- make replacement cols negative - let's us find unmatched items
        update t 
        set t.ItemId = -t.ItemID, t.ItemHierarchyId = (case when -1 = t.ItemHierarchyId then -1 else -t.ItemHierarchyId end)
        from [dbo].raw_LegacyData_TransactionItemSales_Temp_Mapped as t

        select top 100 * from [dbo].raw_LegacyData_TransactionItemSales_Temp_Mapped

        -- map legacy item keys to wh itemnumbers and set hierarchy id to correspondng wh segmentids
        update t
        set t.ItemId = s.ItemNumber, t.ItemHierarchyId = s.SegmentID 
        from [dbo].raw_LegacyData_TransactionItemSales_Temp_Mapped  as t
        inner join WH_Legacy_Item_Xref as s
        on (-t.ItemId) = s.LG_ItemKey;

        select *
        from [dbo].raw_LegacyData_TransactionItemSales_Temp_Mapped where ItemHierarchyId < 0
   ```

6. **Compute hash keys and stage insert** work through script `merge_legacy_mapped.sql`
   
   ```SQL
        /* truncate a landing table and clear contraints */
        truncate table [dbo].stage_ST_TransactionItemSales_Temp;

        /* drop indexes */
        drop index if exists UPK_stage_ST_TransactionItemSales_Temp ON [dbo].stage_ST_TransactionItemSales_Temp;
        drop index if exists UK_stage_ST_TransactionItemSales_Temp_UniqueHashKey ON [dbo].stage_ST_TransactionItemSales_Temp;

        select top 1000 * from stage_ST_TransactionItemSales_Temp

        /* insert incoming transactions  */
        insert into [dbo].stage_ST_TransactionItemSales_Temp
            (
            [UniqueHashKey]
            ,[CompareHashKey]
            ,[buID],[buCode],[TransSequenceNumber],[ItemId],[TransLineNumber],[BusinessDate],[PumpNumber]
            ,[Barcode],[Hose],[ItemGrossSoldAmt],[ItemGrossSoldQty],[ItemHierarchyId],[itemHierExtID5]
            ,[ItemReductionAmt],[ItemRefundAmt],[ItemRefundQty],[ItemRefundReductionAmt],[NetVatAmt]
            ,[originalPriceAmt],[VatNetReductionAmt],[EmployeeId],[SalesDestId],[ShiftId],[TransTimestamp]
            ,[ZipFileName],[ZipRunNumber],[XMLFileName],[XMLFileDate]
            ,TranClass
            )
        select
            HASHBYTES('MD5', 
                CONCAT(
                [buID],[buCode],[TransSequenceNumber],[ItemId],[TransLineNumber],
                [BusinessDate],ISNULL([PumpNumber],-1)
                )
                ) 
                AS [UniqueHashKey],

            HASHBYTES('MD5', 
                CONCAT(
                [Barcode],[Hose],[ItemGrossSoldAmt],[ItemGrossSoldQty],
                [ItemHierarchyId],[itemHierExtID5],[ItemReductionAmt],
                [ItemRefundAmt],[ItemRefundQty],[ItemRefundReductionAmt],
                [NetVatAmt],[originalPriceAmt],[VatNetReductionAmt],
                [EmployeeId],[SalesDestId],[ShiftId],[TransTimestamp]
                )
                ) 
                AS [CompareHashKey],

            [buID], [buCode], [TransSequenceNumber], [ItemId], [TransLineNumber],
            [BusinessDate],
            ISNULL([PumpNumber],-1) AS [PumpNumber],
            [Barcode], [Hose], [ItemGrossSoldAmt], [ItemGrossSoldQty],
            [ItemHierarchyId], [itemHierExtID5], [ItemReductionAmt], [ItemRefundAmt],
            [ItemRefundQty], [ItemRefundReductionAmt], [NetVatAmt], [originalPriceAmt],
            [VatNetReductionAmt], [EmployeeId], [SalesDestId], [ShiftId],
            [TransTimestamp], [ZipFileName], [ZipRunNumber], [XMLFileName], [XMLFileDate],


            datepart(HOUR, isnull(try_cast(TransTimestamp as datetime), cast('1900-01-01 00:00:00' as datetime))) as TranClass
        from
            [dbo].raw_LegacyData_TransactionItemSales_Temp_Mapped;
   ```

   ```SQL
        -- capture max stage key
        declare @MaxKey bigint = (select max(stage_ST_TransactionItemSalesKey) from [EMCS_Staging].[dbo].[stage_ST_TransactionItemSales])
        print(@MaxKey)

        insert into [dbo].stage_ST_TransactionItemSales
            (
            [UniqueHashKey]
            ,[CompareHashKey]
            ,[buID],[buCode],[TransSequenceNumber],[ItemId],[TransLineNumber],[BusinessDate],[PumpNumber]
            ,[Barcode],[Hose],[ItemGrossSoldAmt],[ItemGrossSoldQty],[ItemHierarchyId],[itemHierExtID5],[ItemReductionAmt]
            ,[ItemRefundAmt],[ItemRefundQty],[ItemRefundReductionAmt],[NetVatAmt],[originalPriceAmt],[VatNetReductionAmt]
            ,[EmployeeId],[SalesDestId],[ShiftId],[TransTimestamp],[ZipFileName],[ZipRunNumber],[XMLFileName],[XMLFileDate]
            , CreateDatetime
            , UpdateDatetime
            )
        select
            s.[UniqueHashKey]
            , s.[CompareHashKey]
            , s.[buID], s.[buCode], s.[TransSequenceNumber], s.[ItemId], s.[TransLineNumber], s.[BusinessDate], ISNULL(s.[PumpNumber],-1)
            , s.[Barcode], s.[Hose], s.[ItemGrossSoldAmt], s.[ItemGrossSoldQty], s.[ItemHierarchyId], s.[itemHierExtID5]
            , s.[ItemReductionAmt], s.[ItemRefundAmt], s.[ItemRefundQty], s.[ItemRefundReductionAmt], s.[NetVatAmt]
            , s.[originalPriceAmt], s.[VatNetReductionAmt], s.[EmployeeId], s.[SalesDestId], s.[ShiftId]
            , s.[TransTimestamp], s.[ZipFileName], s.[ZipRunNumber], s.[XMLFileName], s.[XMLFileDate]
            , GETDATE()
            , GETDATE()
        from
            stage_ST_TransactionItemSales_Temp s;
   ```



